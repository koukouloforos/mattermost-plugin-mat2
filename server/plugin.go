package main

import (
	"bufio"
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"sync"

	"github.com/mattermost/mattermost-server/v5/model"
	"github.com/mattermost/mattermost-server/v5/plugin"
)

// Plugin implements the interface expected by the Mattermost server to communicate between the server and plugin processes.
type Plugin struct {
	plugin.MattermostPlugin

	// configurationLock synchronizes access to the configuration.
	configurationLock sync.RWMutex

	// configuration is the active plugin configuration. Consult getConfiguration and
	// setConfiguration for usage.
	configuration *configuration
}

// FileWillBeUploaded is invoked when a file is uploaded, but before it is committed to backing store.
func (p *Plugin) FileWillBeUploaded(c *plugin.Context, info *model.FileInfo, file io.Reader, output io.Writer) (*model.FileInfo, string) {
	// Load plugin configuration
	config := p.getConfiguration()

	// Generic error string
	errstr := " Contact your Mattermost administrator for assistance."

	// Correct extension for compressed tar files
	extension := info.Extension
	if strings.HasSuffix(info.Name, ".tar."+extension) && len(info.Name) > 5+len(extension) {
		extension = "tar." + extension
	}

	// Ignored files types
	ignored := append(strings.Split(config.IgnoredExtensions, ","), "")
	for _, ext := range ignored {
		if ext == extension {
			return info, ""
		}
	}

	// Create temporary directory if missing
	if _, err := os.Stat(config.TempDirectory); os.IsNotExist(err) {
		if err := os.Mkdir(config.TempDirectory, 0700); err != nil {
			p.API.LogError("Unable to create temporary directory for MAT2. " + err.Error())
			return nil, "Unable to create temporary directory for MAT2." + errstr
		}
	}

	// Save uploaded file to temporary directory
	pattern := "mmost-mat2tmp-*" + "." + extension
	tmpfile, err := ioutil.TempFile(config.TempDirectory, pattern)
	defer os.Remove(tmpfile.Name())
	if err != nil {
		p.API.LogError("Unable to create temporary file. " + err.Error())
		return nil, "Unable to create temporary file." + errstr
	}
	writer := bufio.NewWriter(tmpfile)
	if _, err = io.Copy(writer, file); err != nil {
		p.API.LogError("Unable to write to temporary file. " + err.Error())
		return nil, "Unable to write to temporary file." + errstr
	}
	if err = writer.Flush(); err != nil {
		p.API.LogError("Unable to write to temporary file. " + err.Error())
		return nil, "Unable to write to temporary file." + errstr
	}
	if err = tmpfile.Close(); err != nil {
		p.API.LogError("Unable to write to temporary file. " + err.Error())
		return nil, "Unable to write to temporary file." + errstr
	}

	// Call MAT2
	if _, err = os.Stat(config.Mat2Path); os.IsNotExist(err) {
		p.API.LogError("Unable to find MAT2 executable. " + err.Error())
		return nil, "MAT2 is unavailable." + errstr
	}
	cleaned := strings.TrimSuffix(tmpfile.Name(), extension) + "cleaned." + extension
	var cmd *exec.Cmd
	if config.PdfLightweight && (extension == "pdf" || strings.HasSuffix(info.MimeType, "/pdf")) {
		cmd = exec.Command(config.Mat2Path, "-L", tmpfile.Name())
	} else {
		cmd = exec.Command(config.Mat2Path, tmpfile.Name())
	}
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &out
	err = cmd.Run()

	if err != nil {
		p.API.LogWarn("MAT2 failed.", "filename", info.Name, "output", out.String())
	} else {
		// If MAT2 succeeded, update file info and contents
		defer os.Remove(cleaned)
		cleanedfile, err := os.Open(cleaned)
		if err != nil {
			p.API.LogError("Unable to read from cleaned file. " + err.Error())
			return nil, "Unable to read from cleaned file." + errstr
		}
		reader := bufio.NewReader(cleanedfile)
		sz, err := io.Copy(output, reader)
		if err != nil {
			p.API.LogError("Unable to read from cleaned file. " + err.Error())
			return nil, "Unable to read from cleaned file." + errstr
		}
		info.Size = sz
		if err := cleanedfile.Close(); err != nil {
			p.API.LogError("Unable to read from cleaned file. " + err.Error())
			return nil, "Unable to read from cleaned file." + errstr
		}
	}

	return info, ""
}
