module 0xacab.org/koukouloforos/mattermost-plugin-mat2

go 1.12

require (
	github.com/mattermost/mattermost-server/v5 v5.20.0
	github.com/mholt/archiver/v3 v3.3.0
	github.com/pkg/errors v0.9.1
)
