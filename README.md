# MAT2 plugin for Mattermost

This plugin uses [MAT2](https://0xacab.org/jvoisin/mat2) to scrub the metadata
of uploaded files.

**Requirements:**
* Mattermost Server Version: 5.12+
* [MAT2](https://0xacab.org/jvoisin/mat2)

## Building

```
make
```

This will produce a single plugin file (with support for multiple
architectures) for upload to your Mattermost server:

```
dist/org.0xacab.koukouloforos.mattermost-plugin-mat2-0.1.2.tar.gz
```

## Installation

Deploy and upload your plugin via the [System Console](
  https://docs.mattermost.com/administration/plugins.html#custom-plugins).

## Configuration

In the Mattermost System Console, under **System Console > Plugins > MAT2**.
